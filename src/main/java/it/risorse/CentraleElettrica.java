package it.risorse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CentraleElettrica {
    protected int numRisorse;
    protected RisorsaEnergetica[] risorseDisponibili;
    protected double efficienza;

    public CentraleElettrica(int numRisorse, RisorsaEnergetica[] risorseDisponibili, double efficienza) {
        this.numRisorse = numRisorse;
        risorseDisponibili = new RisorsaEnergetica[this.numRisorse];
        this.risorseDisponibili = risorseDisponibili;
        this.efficienza = efficienza;
    }

    public int getNumRisorse() {
        return numRisorse;
    }

    public void setNumRisorse(int numRisorse) {
        this.numRisorse = numRisorse;
    }

    public RisorsaEnergetica[] getRisorseDisponibili() {
        return risorseDisponibili;
    }

    public void setRisorseDisponibili(RisorsaEnergetica[] risorseDisponibili) {
        this.risorseDisponibili = risorseDisponibili;
    }

    public double getEfficienza() {
        return efficienza;
    }

    public void setEfficienza(double efficienza) {
        this.efficienza = efficienza;
    }

    public CentraleElettrica(int numRisorse, double efficienza) {
        this.numRisorse = numRisorse;
        risorseDisponibili = new RisorsaEnergetica[this.numRisorse];
        this.efficienza = efficienza;
    }
    public void aggiungiRisorsa(RisorsaEnergetica risorsa) {
        numRisorse++;
        List risorseDisponibiliList = new ArrayList(Arrays.asList(risorseDisponibili));
        risorseDisponibiliList.add(risorsa);
        risorseDisponibili = (RisorsaEnergetica[]) risorseDisponibiliList.toArray(risorseDisponibili);
        System.out.println("la risorsa " + risorsa.getNome() +" è ora disponibile");
        }

    public void visualizzaRisorse(){
        for (int i = 0; i < numRisorse; i++){
            System.out.println(this.risorseDisponibili[i].getNome()+ " " +
            this.risorseDisponibili[i].getQuantitaDisponibile() + " " +
            this.risorseDisponibili[i].getPotenzaCalorifica() + " " +
            this.risorseDisponibili[i].getPrezzo());
        }
    }
    public double calcolaProduzioneEnergetica(){
        double[] produzione = new double[this.numRisorse];
        double totaleProduzione = 0;
        for (int i = 0; i < numRisorse; i++){
            produzione[i] = this.risorseDisponibili[i].getQuantitaDisponibile() *
                    this.risorseDisponibili[i].getPotenzaCalorifica();}
        for (int i = 0; i < numRisorse; i++){
            totaleProduzione += produzione[i];
        }
        return totaleProduzione * this.efficienza;

    }
    public void simulaConsumo(double consumo){
        if (calcolaProduzioneEnergetica() < consumo){ // controllo se ho abbastanza energia per applicare il consumo
            System.out.println("energia non disponibile");}
        else {
            List risorseDisponibiliList = new ArrayList(Arrays.asList(risorseDisponibili));
            //ciclo sulle risorse disponibili partendo dalla prima, se il consumo è maggiore dell'energia che mi può dare la tolgo
            for (int i = 0; i < risorseDisponibiliList.size(); i++){
                RisorsaEnergetica risorsa = (RisorsaEnergetica) risorseDisponibiliList.get(i);
                if (consumo >= risorsa.getQuantitaDisponibile() * risorsa.getPotenzaCalorifica()){
                    // e scalo la quantità rimossa dal consumo
                    consumo -= risorsa.getQuantitaDisponibile() * risorsa.getPotenzaCalorifica();
                    risorseDisponibiliList.remove(i);
                }
                else{//decremento la quantità dell'ultima risorsa interessata dal consumo
                    double quantitaNecesaria = consumo/risorsa.getPotenzaCalorifica();
                    risorsa.setQuantitaDisponibile(risorsa.getQuantitaDisponibile() - quantitaNecesaria);
                }
            }
        }
    }


}
