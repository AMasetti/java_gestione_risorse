package it.risorse;

public class RisorsaEnergetica {
    enum Nome {
        PETROLIO,
        GAS
    }
    protected Nome nome;

    protected double quantitaDisponibile;
    protected double potenzaCalorifica;
    protected double prezzo;

    public RisorsaEnergetica(Nome nome, double quantitaDisponibile, double potenzaCalorifica, double prezzo) {
        this.nome = nome;
        this.quantitaDisponibile = quantitaDisponibile;
        this.potenzaCalorifica = potenzaCalorifica;
        this.prezzo = prezzo;
    }

    public Nome getNome() {
        return nome;
    }

    public double getQuantitaDisponibile() {
        return quantitaDisponibile;
    }

    public void setQuantitaDisponibile(double quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }

    public double getPotenzaCalorifica() {
        return potenzaCalorifica;
    }

    public double getPrezzo() {
        return prezzo;
    }

    /**
     * metodo che permette di colcolare il costo di una quantità inserita in input di risorsa
     * @param quantita quantità di risorsa da utilizzare
     * @return costo della quantità di risorsa in base al prezzo unitario
     */
    public double calcolaCosto(double quantita){
        return quantita*this.prezzo;
    }



}
