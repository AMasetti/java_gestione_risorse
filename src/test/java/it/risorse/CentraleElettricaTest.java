package it.risorse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static it.risorse.RisorsaEnergetica.Nome.GAS;
import static org.junit.jupiter.api.Assertions.*;

class CentraleElettricaTest {

    static CentraleElettrica centrale;

    //@BeforeEach;
    void setUp(){
        centrale = new CentraleElettrica(0, 0.7);
    }

    @Test
    void aggiungiRisorsa() {
        RisorsaEnergetica risorsa1 = new RisorsaEnergetica(GAS, 100, 20, 1.5);
        CentraleElettrica centrale = new CentraleElettrica(0, 0.7);
        centrale.aggiungiRisorsa(risorsa1);
        assertEquals(1, centrale.getRisorseDisponibili().length);
    }


    @Test
    void calcolaProduzioneEnergetica() {
        RisorsaEnergetica risorsa1 = new RisorsaEnergetica(GAS, 100, 20, 1.5);
        RisorsaEnergetica risorsa2 = new RisorsaEnergetica(GAS, 50, 10, 1.5);

        CentraleElettrica centrale = new CentraleElettrica(0, 0.7);

        centrale.aggiungiRisorsa(risorsa1);

        assertEquals(1400, centrale.calcolaProduzioneEnergetica());

        centrale.aggiungiRisorsa(risorsa2);

        assertEquals(1750, centrale.calcolaProduzioneEnergetica());
    }

    @Test
    void simulaConsumo() {
        RisorsaEnergetica risorsa1 = new RisorsaEnergetica(GAS, 100, 20, 1.5);
        RisorsaEnergetica risorsa2 = new RisorsaEnergetica(GAS, 50, 10, 1.5);

        CentraleElettrica centrale = new CentraleElettrica(0, 0.7);

        centrale.aggiungiRisorsa(risorsa1);
        centrale.aggiungiRisorsa(risorsa2);

        centrale.simulaConsumo(350);
        assertEquals(1400, centrale.calcolaProduzioneEnergetica());
    }
}